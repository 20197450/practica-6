﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {

        /* Crear una clase Contacto. Esta clase deberá tener los atributos
        "nombre, apellidos, telefono y direccion". También deberá tener un método
        "SetContacto", de tipo void y con los parámetros string, que permita cambiar
        el valor de los atributos. También tendrá un método "Saludar", que escribirá en
        pantalla "Hola, soy " seguido de sus datos. Crear también una clase llamada ProbarContacto.
        Esta clase deberá contener sólo la función Main, que creará dos objetos de tipo Contacto, les
        asignará los datos del contacto y les pedirá que saluden.*/



        public class Contacto
        {
            public string Nombre;
            public string Apellido;
            public string Telefono;
            public string Dirección;



            public void SetContacto()
            {
                Nombre = Console.ReadLine();
                Apellido = Console.ReadLine();
                Telefono = Console.ReadLine();
                Dirección = Console.ReadLine();


            }

            public void Saludar()
            {



                Console.WriteLine($"\nSaludo, soy {Nombre} {Apellido}, mi numero de telefono es {Telefono} y vivo en {Dirección}");
                Console.WriteLine($"////////////////////////////////////////////////////////////////////////////////////////////////\n");




            }

        }
        public class ProbarContacto
        {
            static void Main(string[] args)
            {
                Contacto Contacto1 = new Contacto();

                Console.WriteLine("////////////////////////////////////////////////////////////////////////////");
                Console.WriteLine("Por favor introduzca los datos:\nNombre.\nApellido.\nNumero.\nDireccion.\n");
                Contacto1.SetContacto();
                Contacto1.Saludar();

                Contacto Contacto2 = new Contacto();
                Console.WriteLine("Por favor introduzca los datos:\nNombre.\nApellido.\nNumero.\nDireccion.\n");
                Contacto2.SetContacto();
                Contacto2.Saludar();

                Console.ReadKey();


            }
        }
    }
}
