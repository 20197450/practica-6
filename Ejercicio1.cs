﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{

    /*Crear una clase Persona que tenga como atributos el
    "cedula, nombre, apellido y la edad (definir las propiedades para poder acceder a dichos atributos)".
    Definir como responsabilidad un método para mostrar ó imprimir. Crear una segunda clase Profesor que
    herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo.
    Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la
    clase Profesor y llamar a sus métodos y propiedades.*/




    class Persona
    {
        public int cedula = 412345678;
        public string Apellido = "Gonzalez Paulino";
        public string Nombre = "Jeuri Harlen";
        public byte edad = 20;

        public void Imprimirporconsola()
        {


            Console.WriteLine($"Información de la persona: Cedula:{cedula}, Apellido: {Apellido}, Nombre: {Nombre}, Edad: {edad}");
            Console.WriteLine($"//////////////////////////////////////////////////////////////////////////////////////////////////\n");

        }

    }

    class Profesor:Persona
    {
        int SALARIO = 20000;
        public void Escribir()
        {



            Console.WriteLine("El salario del docente es: " + SALARIO);


        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Persona PERSONA1 = new Persona();
            Console.WriteLine("informacion privada de la persona");
            PERSONA1.Imprimirporconsola();

            Profesor Profesor1 = new Profesor();
            Console.WriteLine("Información privada del Docente");
            Profesor1.cedula = 412345678;
            Profesor1.Apellido = "Perez";
            Profesor1.Nombre = "Emiliana";
            Profesor1.edad = 48;
            Profesor1.Imprimirporconsola();
            Profesor1.Escribir();

            Console.ReadKey();
        }
    }

}