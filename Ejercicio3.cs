﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{

    /*Crear tres clases ClaseA,
    ClaseB, ClaseC que ClaseB herede de ClaseA
    y ClaseC herede de ClaseB. Definir un constructor
    a cada clase que muestre un mensaje. Luego definir
    un objeto de la clase ClaseC.*/



    public class ClaseA
    {
        public ClaseA()
        {
            Console.WriteLine("Constructor clase A");
        }
    }

    public class ClaseB:ClaseA
    {
        public ClaseB()
        {
            Console.WriteLine("Constructor clase  B");
        }
    }

    public class ClaseC:ClaseB
    {
        public ClaseC()
        {
            Console.WriteLine("Constructor clase C");
            
        }
    }

    class Prueba
    {
        static void Main(string[] args)
        {
            ClaseC obj1 = new ClaseC();
            Console.ReadKey();
        }
    }


}
